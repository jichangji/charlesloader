## **Charles v4.2版本分析记录**

--by B.S.

October 6, 2017 10:08 PM

##### 最新版v4.2相对v.4.0.2比较，明文类名已混淆.

> https://www.charlesproxy.com/assets/release/4.2/charles-proxy-4.2-win64.msi
>
> https://www.charlesproxy.com/assets/release/4.2/charles-proxy-4.2-win32.msi
>
> https://www.charlesproxy.com/assets/release/4.2/charles-proxy-4.2.dmg
>
> https://www.charlesproxy.com/assets/release/4.2/charles-proxy-4.2_amd64.tar.gz
>
> https://www.charlesproxy.com/assets/release/4.2/charles-proxy-4.2.tar.gz
>

##### 反编译为源码后,找关键点的分析流程:

###### 这里414试一试另外一种更简单的方法:

> 1. 在com.xk72.charles.gui.SplashWindow.java里找到
> public void showRegistrationStatus()函数方法;
``` java
public void showRegistrationStatus() {
        if (WNzU.OjEP()) {
            this.showStatus("Registered to: " + WNzU.DZZn());
            return;
        }
        this.showSharewareStatus();
    }
```
2. 在if()条件里的,是否注册成功 WNzU.OjEP() 函数和 "Registered to: " 后面的注册给谁的WNzU.DZZn() 函数,都来自 import com.xk72.charles.WNzU 类;
3. 在com.xk72.charles.WNzU 就是关键类。修改掉上面两个函数即可。


> hook掉public static boolean x1()和public static java.lang.String x3()两个方法函数就可以了,当然最好public修饰的都hook掉;

##### 只需要在用javassist时,设置这一句就可以将破解好的class类文件就会dump出来
> CtClass.debugDump = "./charles-bs-cr";
