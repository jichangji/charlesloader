/**
 * 
 */
package xyz.allin1.charlesloader;

import java.lang.instrument.Instrumentation;

/**
 * @author iYoungDone
 *
 */
public class CharlesAgent {

	/**
	 * 
	 */
	public CharlesAgent() {
		// TODO Auto-generated constructor stub
	}
	public static void premain(String s, Instrumentation instrumentation)
    {
        System.out.println("=========查尔斯动态加载启动器(CharlesLoader)========");
        System.out.println("=========Digital Agent B.S.(Coded by iDone)========");
        System.out.println("=========http://www.appleos.xyz========");
        System.out.println("=========http://www.allin1.xyz========");
        
        System.out.printf("\033[%d;%d;%dm[allin1.xyz出品]!!!\033[0m", 4, 34, 42);
        System.out.printf("\n");
        
        // 注册变形金刚O(∩_∩)O~Transformer
        instrumentation.addTransformer(new CharlesMonitorTransformer());
    }

}
