## **Charles v4.1.4版本分析记录**

--by B.S.

July 18, 2017 5:25 PM

##### 最新版v4.1.4相对v.4.0.2比较，明文类名已混淆.

##### 反编译为源码后,找关键点的分析流程:

###### 这里414试一试另外一种更简单的方法:

> 1. 在com.xk72.charles.gui.SplashWindow.java里找到
> public void showRegistrationStatus()函数方法;
``` java
public void showRegistrationStatus() {
    if (gIbD.xUFT()) {
        this.showStatus("Registered to: " + gIbD.PcqR());
        return;
    }
    this.showSharewareStatus();
}
```
2. 在if()条件里的,是否注册成功gIbD.xUFT()函数和"Registered to: "后面的注册给谁的gIbD.PcqR()函数,都来自import com.xk72.charles.gIbD类
3. 在com.xk72.charles.gIbD就是关键类。修改掉上面两个函数即可。


> hook掉public static boolean x1()和public static java.lang.String x3()两个方法函数就可以了,当然最好public修饰的都hook掉;

##### 只需要在用javassist时,设置这一句就可以将破解好的class类文件就会dump出来
> CtClass.debugDump = "./charles-bs-cr";
